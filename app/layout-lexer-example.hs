module Main (main) where

import           Data.Foldable
import           System.Environment
import           System.Exit

import           LayoutLexerTypes
import           LayoutLexer

main :: IO ()
main = do
  args <- getArgs
  path <- case args of
    x : _    -> pure x
    []       -> do
      prog <- getProgName
      putStrLn $ prog <> " FILE"
      exitFailure
  src <- readFile path
  ts <- case runAlex src scanAll of
    Left e -> do
      putStrLn e
      exitFailure
    Right v -> pure v
  traverse_ print ts

scanAll :: Alex [Token]
scanAll = do
  t <- alexMonadScan
  case t of
    TEof -> pure []
    _    -> fmap (t :) scanAll
