{
module LayoutLexer where

import           LayoutLexerTypes
}

%wrapper "monadUserState"

$white_no_nl = $white # \n
$alpha = a-z
$alphanum = [a-z0-9]

@ident = $alpha $alphanum*


tokens :-

$white_no_nl+                           ;

<layout> {
  \n                                    ;
  \{                                    { explicitLBrace }
  ()                                    { newLayoutContext }
}

<0> {

  \n                                    { beginCode bol }

  "where"                               { simple TWhere }

  @ident                                { simpleString TIdent }

  \{                                    { simple TLBrace }
  \}                                    { simple TRBrace }
  \;                                    { simple TSemi }

}

<bol> {
  \n                                    ;
  ()                                    { doBol }
}

{

beginCode :: Int -> AlexAction Token
beginCode n _ _ = do
  pushLexState n
  alexMonadScan

simple :: Token -> AlexAction Token
simple t = \_ _ -> do
  case t of
    TWhere -> do
      pushLexState layout
    _      -> pure ()
  pure t

simpleString :: (String -> a) -> AlexAction a
simpleString f (_, _, _, s) len = pure (f (take len s))

alexEOF :: Alex Token
alexEOF = do
  l <- getLayout
  case l of
    Just (Layout _) -> do
      _ <- popLayout
      pure TVRBrace
    Just  NoLayout  -> do
      _ <- popLayout
      alexMonadScan
    Nothing -> pure TEof

newLayoutContext :: AlexAction Token
newLayoutContext _ _ = do
  _ <- popLexState
  offset <- getOffset
  pushLayout $ Layout offset
  pure TVLBrace

explicitLBrace :: AlexAction Token
explicitLBrace _ _ = do
  _ <- popLexState
  pushLayout NoLayout
  pure TLBrace

doBol :: AlexAction Token
doBol _ _ = do
  offset <- getOffset
  layoutContext <- getLayout
  case layoutContext of
    Just (Layout n) -> case offset `compare` n of
      LT -> do
        _ <- popLayout
        pure TVRBrace
      EQ -> do
        _ <- popLexState
        pure TVSemi
      GT -> do
        _ <- popLexState
        alexMonadScan
    _               -> do
      _ <- popLexState
      alexMonadScan

getOffset :: Alex Int
getOffset = do
  (AlexPn _ _ column, _, _, _) <- alexGetInput
  pure column

getLayout :: Alex (Maybe LayoutContext)
getLayout = do
  AUS{ layoutStack = lcs } <- alexGetUserState
  case lcs of
    []     -> pure Nothing
    lc : _ -> pure (Just lc)

pushLayout :: LayoutContext -> Alex ()
pushLayout lc = do
  s@AUS{ layoutStack = lcs } <- alexGetUserState
  alexSetUserState s{ layoutStack = lc : lcs }

popLayout :: Alex LayoutContext
popLayout = do
  s@AUS{ layoutStack = lcs } <- alexGetUserState
  case lcs of
    []       -> alexError "layout expected but no layout available"
    lc : lcs' -> do
      alexSetUserState s{ layoutStack = lcs' }
      pure lc

pushLexState :: Int -> Alex ()
pushLexState nsc = do
  sc <- alexGetStartCode
  s@AUS{ alexStartCodes = scs } <- alexGetUserState
  alexSetUserState s{ alexStartCodes = sc : scs }
  alexSetStartCode nsc

popLexState :: Alex Int
popLexState = do
  csc <- alexGetStartCode
  s@AUS{ alexStartCodes = scs } <- alexGetUserState
  case scs of
    []       -> alexError "state code expected but no state code available"
    sc : scs' -> do
      alexSetUserState s{ alexStartCodes = scs' }
      alexSetStartCode sc
      pure csc
}
