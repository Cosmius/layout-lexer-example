module LayoutLexerTypes where

data Token
  = TEof
  | TWhere
  | TSemi
  | TLBrace
  | TRBrace
  | TVSemi
  | TVLBrace
  | TVRBrace
  | TIdent String
  deriving Show

data LayoutContext
  = NoLayout
  | Layout Int
  deriving Show

data AlexUserState
  = AUS
    { layoutStack    :: [LayoutContext]
    , alexStartCodes :: [Int]
    }
  deriving Show

alexInitUserState :: AlexUserState
alexInitUserState = AUS
  { layoutStack    = []
  , alexStartCodes = []
  }
